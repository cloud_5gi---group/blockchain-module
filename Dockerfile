FROM node:8

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn

RUN mkdir /home/app
WORKDIR /home/app

COPY package.json ./
RUN npm install

ENV NETWORK_URL="http://localhost:3001"

COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]