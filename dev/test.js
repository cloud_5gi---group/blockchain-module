//  const Blockchain = require('./blockchain')
const level = require('level')

//  const bitcoin = new Blockchain()

/* const currentBlockData = [
  {
    amount: 2500,
    sender: 'SDJLQKSDJQSD45',
    recipient: 'KJLJDQSD4QS5D4'
  },
  {
    amount: 1000,
    sender: 'SDELQKSDJQSD45',
    recipient: 'KJLJSQSD4QS5D4'
  },
  {
    amount: 1000,
    sender: 'SDELQKSDJQSD45',
    recipient: 'KJLJSQSD4QS5D4'
  }
]
 */
const previousBlockHash = 'SDJKLQSJDLKQSJD45s512'

/* const nonce = bitcoin.proofOfWork(previousBlockHash, currentBlockData)
const hash = bitcoin.hashBlock(previousBlockHash, currentBlockData, nonce) */

const db = level('./testdb', { valueEncoding: 'json' })
const nonce = 1
const hash = '0SDSD54SDSDSD42124SD5'
const blocks = [
  {
    type: 'put',
    key: hash,
    value: {
      index: 1,
      timestamp: Date.now(),
      transaction: {
        partiePolitique: 'test', candidat1: 0.75, candidat2: 0.45
      },
      nonce: nonce, //  Proof of work
      hash: hash, //  data of the block
      previousBlockHash: previousBlockHash // Previous data of the blockchain
    }
  },
  {
    type: 'put',
    key: hash + 'E',
    value: {
      index: 2,
      timestamp: Date.now(),
      transaction: {
        partiePolitique: 'test', candidat1: 0.75, candidat2: 0.45
      },
      nonce: nonce, //  Proof of work
      hash: hash + 'E', //  data of the block
      previousBlockHash: previousBlockHash // Previous data of the blockchain
    }
  },
  {
    type: 'put',
    key: hash + 'F',
    value: {
      index: 4,
      timestamp: Date.now(),
      transaction: {
        partiePolitique: 'test', candidat1: 0.75, candidat2: 0.45
      },
      nonce: nonce, //  Proof of work
      hash: hash + 'F', //  data of the block
      previousBlockHash: previousBlockHash // Previous data of the blockchain
    }
  }
]

db.batch(blocks, function (err) {
  if (err) console.log(err)
  console.log('block added')
})

const stream = db.createReadStream()

stream.on('data', function (data) {
  const value = data.value
  console.log(value)
})

stream.on('closed', function (err) {
  if (err) console.log(err)
  console.log('stream closed')
})

stream.on('end', function (err) {
  if (err) console.log(err)
  console.log('stream end')
})
