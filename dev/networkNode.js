const express = require('express');
require('dotenv/config');
let app;
app = express();
const port = process.env.PORT||3000;
const bodyParser = require('body-parser');
const Blockchain = require('./blockchain');
const rp = require('request-promise');

const cloudProjectBlockChain = new Blockchain();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/*  Get All The BlockChain */
app.get('/blockchain', (req, res) => {
  res.json(cloudProjectBlockChain)
});

/*  To Create a new Transaction */
app.post('/transaction', (req, res) => {
  const newTransaction = req.body;
  console.log(newTransaction);
  const blockIndex = cloudProjectBlockChain.addTransactionToPendingTransaction(newTransaction);
  res.json({
    message: 'PV will be added on block ' + blockIndex
  })
});

/*  To Create a new block */
app.get('/mine', (req, res) => {
  const lastBlock = cloudProjectBlockChain.getLastBlock();
  const previousBlockHash = lastBlock.hash;
  const currentBlockData = {
    transactions: cloudProjectBlockChain.pendingTransaction,
    index: lastBlock.index + 1
  };

  const nonce = cloudProjectBlockChain.proofOfWork(previousBlockHash, currentBlockData);
  const hash = cloudProjectBlockChain.hashBlock(previousBlockHash, currentBlockData, nonce);

  const newBlock = cloudProjectBlockChain.createNewBlock(nonce, previousBlockHash, hash);

  const requestPromises = [];
  cloudProjectBlockChain.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: networkNodeUrl + 'receive-new-block',
      method: 'POST',
      body: {
        newBlock: newBlock
      },
      json: true
    };

    requestPromises.push(rp(requestOptions))
  });

  Promise.all(requestPromises).then(data => {
    res.json({
      message: 'Le Block a été crée et partagé dans tout le réseau avec succès',
      newBlock: newBlock
    })
  })
});

/*  Register A Node And BroadCast That Node To The Entire Networ */
app.post('/register-and-broadcast-node', (req, res) => {
  const newNodeUrl = req.body.newNodeUrl;
  const notAlreadyRegister = cloudProjectBlockChain.networkNodes.indexOf(newNodeUrl) === -1;
  if (notAlreadyRegister) cloudProjectBlockChain.networkNodes.push(newNodeUrl);

  const regNodePromises = [];
  cloudProjectBlockChain.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: networkNodeUrl + '/register-node',
      method: 'POST',
      body: { newNodeUrl: newNodeUrl },
      json: true
    };

    regNodePromises.push(rp(requestOptions))
  });

  Promise.all(regNodePromises).then(data => {
    const bulkRequestOptions = {
      uri: newNodeUrl + 'register-nodes-bulk',
      method: 'POST',
      body: { allNetworkNodes: [...cloudProjectBlockChain.networkNodes, newNodeUrl] },
      json: true
    };

    return rp(bulkRequestOptions)
  }).then(data => {
    res.json({
      message: 'Vous avez bien été enregistré au reseau'
    })
  })
});

/*  Register A Node With The Network */
app.post('/register-node', (req, res) => {
  const newNodeUrl = req.body.newNodeUrl;
  const notAlreadyRegister = cloudProjectBlockChain.networkNodes.indexOf(newNodeUrl) === -1;
  const notCurrentNode = newNodeUrl !== cloudProjectBlockChain.currentNodeUrl;

  if (notAlreadyRegister && notCurrentNode) cloudProjectBlockChain.networkNodes.push(newNodeUrl);

  res.json({
    message: 'Votre Noeud a bien été enregistré'
  })
});

/*  Register Multiple Node at Once */
app.post('/register-nodes-bulk', (req, res) => {
  const allNetworkNodes = req.body.allNetworkNodes;

  allNetworkNodes.forEach(networkNodeUrl => {
    const notAlreadyRegister = cloudProjectBlockChain.networkNodes.indexOf(networkNodeUrl) === -1;
    const notCurrentNode = networkNodeUrl !== cloudProjectBlockChain.currentNodeUrl;
    if (notAlreadyRegister && notCurrentNode) cloudProjectBlockChain.networkNodes.push(networkNodeUrl)
  });

  res.json({
    message: 'Bulk Registration Successfully'
  })
});

/* Created a transaction and Broadcast to all the network */
app.post('/transaction/broadcast', (req, res) => {
  const transaction = req.body;
  const newTransaction = cloudProjectBlockChain.createNewTransaction(transaction);

  cloudProjectBlockChain.addTransactionToPendingTransaction(newTransaction);

  const requestPromises = [];
  cloudProjectBlockChain.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: networkNodeUrl + '/transaction',
      method: 'POST',
      body: { newTransaction: newTransaction },
      json: true
    };
    requestPromises.push(rp(requestOptions))
  });

  Promise.all(requestPromises).then(data => {
    res.json({
      message: 'Transaction created and Broadcast successufully'
    })
  })
});

app.post('/receive-new-block', (req, res) => {
  const { newBlock } = req.body;
  const lastBlock = cloudProjectBlockChain.getLastBlock();
  const correctHash = lastBlock.hash === newBlock.previousBlockHash;
  const correctIndex = lastBlock.index + 1 === newBlock.index;

  if (correctHash && correctIndex) {
    cloudProjectBlockChain.chain.push(newBlock);
    cloudProjectBlockChain.pendingTransaction = [];

    res.json({
      message: 'New Block Receive And Accepted'
    })
  } else {
    res.json({
      message: 'New Block Rejected',
      newBlock: newBlock
    })
  }
});

app.get('/consensus', (req, res) => {
  const requestPromises = [];
  cloudProjectBlockChain.networkNodes.forEach(networkNodeUrl => {
    const requestOptions = {
      uri: networkNodeUrl + '/blockchain',
      method: 'GET',
      json: true
    };

    requestPromises.push(rp(requestOptions))
  });

  Promise.all(requestPromises).then(blockchains => {
    const currentChainLength = cloudProjectBlockChain.chain.length;
    let maxChainLength = currentChainLength;
    let newLongestChain = null;
    let newPendingTransactions = [];
    blockchains.forEach(blochchain => {
      if (blochchain.chain.length > maxChainLength) {
        maxChainLength = blochchain.chain.length;
        newLongestChain = blochchain.chain;
        newPendingTransactions = blochchain.pendingTransaction
      }
    });

    if (!newLongestChain || (newLongestChain && !cloudProjectBlockChain.chainIsValid(newLongestChain))) {
      res.json({
        message: 'Current Chain Has Not Been Replaced',
        chain: cloudProjectBlockChain.chain
      })
    } else {
      //  La on doit remplacer la chaine du noeud courant
      cloudProjectBlockChain.chain = newLongestChain;
      cloudProjectBlockChain.pendingTransaction = newPendingTransactions;

      res.json({
        message: 'Current Chain Has Been Replaced',
        newChain: cloudProjectBlockChain.chain
      })
    }
  })
});

app.listen(port, (err) => {
  if (err) console.error(err);
  console.log(`Server Running on port: ' + ${port}`)
});
