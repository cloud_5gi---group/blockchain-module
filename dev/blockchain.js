const sha256 = require('sha256');
require('dotenv/config');

const currentNodeUrl = process.env.NETWORK_URL || 'http://localhost:3001';
const uuid = require('uuid/v1');

const transactionId = uuid().split('-').join('');

class Blockchain {
  constructor () {
    this.chain = []; //  List of Block
    this.pendingTransaction = []; // List of pending transaction
    this.currentNodeUrl = currentNodeUrl;
    this.networkNodes = [];

    this.createNewBlock(100, '0', '0');

    console.log(this)
  }

  createNewBlock (nonce, previousBlockHash, hash) {
    const newBlock = {
      index: this.chain.length + 1,
      timestamp: Date.now(),
      transaction: this.pendingTransaction,
      nonce: nonce, //  Proof of work
      hash: hash, //  data of the block
      previousBlockHash: previousBlockHash // Previous data of the blockchain
    };

    this.pendingTransaction = [];
    this.chain.push(newBlock);

    return newBlock
  }

  getLastBlock () {
    return this.chain[this.chain.length - 1]
  }

  createNewTransaction (bureauVote, nombreInscrit, nombreParticipants, owner, suffrages) {
	  return {
	    bureauVote: bureauVote,
	    nombreInscrit: nombreInscrit,
	    nombreParticipants: nombreParticipants,
	    owner: owner,
	    suffrages: suffrages
    }
  }

  addTransactionToPendingTransaction (newTransaction) {
    this.pendingTransaction.push(newTransaction);
    return this.getLastBlock()['index'] + 1
  }

  hashBlock (previousBlockHash, currentBlockData, nonce) {
    const dataAsString =
      previousBlockHash + nonce.toString() + JSON.stringify(currentBlockData);

    const hash = sha256(dataAsString);

    return hash
  }

  /*   @description: This Function will hash the currenctBlockData
       until the block data verify our condition
       @params: Object currentBlockData, string previousBlockHash
  */
  proofOfWork (previousBlockHash, currentBlockData) {
    let nonce = 0;
    let hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);

    while (hash.substring(0, 4) !== '0000') {
      nonce++;
      hash = this.hashBlock(previousBlockHash, currentBlockData, nonce)
    }

    return nonce
  }

  /*  To See if a blockchain is valid or not */
  chainIsValid (blockchain) {
    let validChain = true;

    for (var i; i < blockchain.length; i++) {
      const currentBlock = blockchain[i];
      const previousBlock = blockchain[i - 1];
      const blockHash = this.hashBlock(previousBlock.hash, {
        transactions: currentBlock.pendingTransaction,
        index: currentBlock.index }, currentBlock.nonce);

      if (blockHash.substring(0, 4) !== '0000') validChain = false;
      if (previousBlock.previousBlockHash !== currentBlock.hash) validChain = false
    }

    const genesisBlock = blockchain[0];
    const correctNonce = genesisBlock.nonce === 100;
    const correctPreviousBlockHash = genesisBlock.previousBlockHash === '0';
    const correctHash = genesisBlock.hash === '0';
    const correctTransaction = genesisBlock.length === 0;

    if (!correctNonce || !correctPreviousBlockHash || !correctHash || !correctTransaction) validChain = false;

    return validChain
  }
}

module.exports = Blockchain;
